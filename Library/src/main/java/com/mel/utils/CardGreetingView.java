package com.mel.utils;

import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.BounceInterpolator;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;

import com.google.android.material.card.MaterialCardView;
import com.mel.utils.cardgreetingview.BuildConfig;
import com.mel.utils.cardgreetingview.R;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

public class CardGreetingView extends MaterialCardView {
    private static final String TAG = CardGreetingView.class.getSimpleName();
    private ConstraintLayout contentView;
    private Paint contentPaint;
    private Path mPath = new Path();
    private AppCompatImageView ivSun;
    private ObjectAnimator animator;
    private TimeInterpolator interpolator;
    private long duration;
    private RectF mRectF = new RectF();
    private Drawable dayImage, nightImage;
    private String text;
    private ColorStateList textColorDay, textColorNight;
    private TextView tv;
    private boolean animateGreeting;
    private long animateGreetingDuration;
    private float bottomPercent, rightPercent, topPercent, leftPercent;
    private int mTextSize, mTypeFace;

    public CardGreetingView(@NonNull Context context) {
        super(context);
    }

    public CardGreetingView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public CardGreetingView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        if (attrs == null) {
            throw new IllegalArgumentException("AttributeSet must be defined");
        }
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CardGreetingView);
        dayImage = typedArray.getDrawable(R.styleable.CardGreetingView_card_dayImage);
        nightImage = typedArray.getDrawable(R.styleable.CardGreetingView_card_nightImage);
        duration = typedArray.getInt(R.styleable.CardGreetingView_duration, 1500);
        mTextSize = typedArray.getDimensionPixelSize(R.styleable.CardGreetingView_textSize, 12);
        textColorDay = typedArray.getColorStateList(R.styleable.CardGreetingView_textColorDay);
        textColorNight = typedArray.getColorStateList(R.styleable.CardGreetingView_textColorNight);
        mTypeFace = typedArray.getInt(R.styleable.CardGreetingView_textStyle, 0);
        text = typedArray.getString(R.styleable.CardGreetingView_text);
        animateGreeting = typedArray.getBoolean(R.styleable.CardGreetingView_animateGreeting, false);
        animateGreetingDuration = typedArray.getInt(R.styleable.CardGreetingView_animateGreetingDuration, 1500);
        bottomPercent = typedArray.getFloat(R.styleable.CardGreetingView_route_bottomPercent, 90);
        rightPercent = typedArray.getFloat(R.styleable.CardGreetingView_route_rightPercent, 90);
        topPercent = typedArray.getFloat(R.styleable.CardGreetingView_route_topPercent, 10);
        leftPercent = typedArray.getFloat(R.styleable.CardGreetingView_route_leftPercent, 10);
        typedArray.recycle();

        initializeElements();
        configureGreeting();

        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(contentView);
        constraintSet.connect(ivSun.getId(), ConstraintSet.LEFT, getId(), ConstraintSet.LEFT, ConstraintSet.PARENT_ID);
        constraintSet.connect(ivSun.getId(), ConstraintSet.RIGHT, getId(), ConstraintSet.RIGHT, ConstraintSet.PARENT_ID);
        constraintSet.connect(ivSun.getId(), ConstraintSet.BOTTOM, getId(), ConstraintSet.BOTTOM, ConstraintSet.PARENT_ID);
        constraintSet.connect(ivSun.getId(), ConstraintSet.TOP, getId(), ConstraintSet.TOP, ConstraintSet.PARENT_ID);

        if (text != null) {
            constraintSet.connect(tv.getId(), ConstraintSet.LEFT, getId(), ConstraintSet.LEFT, ConstraintSet.PARENT_ID);
            constraintSet.connect(tv.getId(), ConstraintSet.RIGHT, getId(), ConstraintSet.RIGHT, ConstraintSet.PARENT_ID);
            constraintSet.connect(tv.getId(), ConstraintSet.BOTTOM, getId(), ConstraintSet.BOTTOM, ConstraintSet.PARENT_ID);
            constraintSet.connect(tv.getId(), ConstraintSet.TOP, getId(), ConstraintSet.TOP, ConstraintSet.PARENT_ID);
        }
        constraintSet.applyTo(contentView);

        post(() -> {
            if (animateGreeting)
                startTextAnimation();
            startAnimation();
        });
    }

    private void initializeElements() {
        setId(getId() == -1 ? View.generateViewId() : getId());
        if (isInEditMode()) {
            contentPaint = new Paint();
            contentPaint.setStrokeWidth(4f);
            contentPaint.setColor(Color.BLACK);
            contentPaint.setStyle(Paint.Style.STROKE);
            contentPaint.setPathEffect(new DashPathEffect(new float[]{10, 20}, 0));
        }
        contentView = new ConstraintLayout(getContext()) {
            @Override
            protected void onDraw(Canvas canvas) {
                super.onDraw(canvas);
                if (isInEditMode()) {
                    mRectF.left = ((leftPercent * getWidth()) / 100);
                    mRectF.top = ((topPercent * getHeight()) / 100) /*- ivSun.getHeight()*/;
                    mRectF.right = ((rightPercent * getWidth()) / 100) /*- ivSun.getWidth()*/;
                    mRectF.bottom = ((bottomPercent * getHeight()) / 100) /*- ivSun.getHeight()*/;
                    mPath.reset();
                    mPath.arcTo(mRectF, 180f, 180f);
                    ivSun.setX(mRectF.left - (float) (ivSun.getWidth() / 2));
                    ivSun.setY((mRectF.top + (mRectF.bottom - mRectF.top) / 2) - ((float) ivSun.getHeight() / 2));
                    canvas.drawPath(mPath, contentPaint);
                }
            }
        };
        contentView.setClipToPadding(true);
        contentView.setWillNotDraw(false);
        contentView.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        contentView.setId(getId() == -1 ? View.generateViewId() : getId());
        addView(contentView);

        contentView.setBackground(dayImage);
        ivSun = new AppCompatImageView(getContext());
        ivSun.setId(View.generateViewId());
        ivSun.setLayoutParams(new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT));
        ivSun.setBackgroundResource(R.drawable.ic_sun);
        contentView.addView(ivSun);
    }

    private void configureGreeting() {
        if (!TextUtils.isEmpty(text)) {
            tv = new TextView(getContext());
            tv.setId(View.generateViewId());
            tv.setText(String.format(Locale.getDefault(), getContext().getString(R.string.str_greeting), getContext().getString(R.string.str_day), text));
            tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, mTextSize);
            tv.setGravity(Gravity.CENTER);

            if (textColorDay != null)
                tv.setTextColor(textColorDay);

            tv.setTypeface(null, mTypeFace == 0 ? Typeface.NORMAL : mTypeFace == 1 ? Typeface.BOLD : Typeface.ITALIC);
            tv.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            contentView.addView(tv);
        }
    }

    private void startTextAnimation() {
        ObjectAnimator textAnimator = ObjectAnimator.ofFloat(tv, "Alpha", 0f, 1f);
        textAnimator.setDuration(animateGreetingDuration);
        textAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        textAnimator.start();
    }

    private void startAnimation() {
        if (animator == null) {
            mRectF.left = ((leftPercent * getWidth()) / 100) - ((float) ivSun.getWidth() / 2);
            mRectF.top = ((topPercent * getHeight()) / 100) - ((float) ivSun.getHeight() / 2);
            mRectF.right = ((rightPercent * getWidth()) / 100) - ((float) ivSun.getWidth() / 2);
            mRectF.bottom = ((bottomPercent * getHeight()) / 100) - ((float) ivSun.getHeight() / 2);
            mPath.reset();

            if (BuildConfig.DEBUG) {
                Log.i(TAG, "Angle -->" + getAngle());
            }

            mPath.arcTo(mRectF, 180f, getAngle());

            animator = ObjectAnimator.ofFloat(ivSun, View.X, View.Y, mPath);

            setDuration(duration);
            animator.setDuration(duration);

            if (interpolator == null) {
                setInterpolator(new BounceInterpolator());
            }
            animator.setInterpolator(interpolator);
        }
        animator.start();
    }

    private float getAngle() {
        Calendar calendar = GregorianCalendar.getInstance();

        if (calendar.get(Calendar.HOUR_OF_DAY) >= 6 && calendar.get(Calendar.HOUR_OF_DAY) < 18) {
            configureTextDay(calendar.get(Calendar.HOUR_OF_DAY));

            if (dayImage != null)
                contentView.setBackground(dayImage);
            ivSun.setBackgroundResource(R.drawable.ic_sun);
        } else {
            configureTextNight();

            if (nightImage != null)
                contentView.setBackground(nightImage);
            ivSun.setBackgroundResource(R.drawable.ic_moon);
        }

        if (calendar.get(Calendar.HOUR_OF_DAY) >= 19)
            calendar.set(Calendar.HOUR, calendar.get(Calendar.HOUR) - 7);
        else
            calendar.set(Calendar.HOUR, calendar.get(Calendar.HOUR) - 6);


        return getAngleForHours(calendar.get(Calendar.HOUR)) +
                getAngleForMinutes(calendar.get(Calendar.MINUTE));
    }

    private float getAngleForHours(int hour) {
        return (float) (hour * 180) / 12;
    }

    private float getAngleForMinutes(int minutes) {
        return (float) (minutes * 15) / 60;
    }

    private void configureTextDay(int hourOfDay) {
        if (tv == null)
            return;

        if (textColorDay != null) {
            tv.setTextColor(textColorDay);
        }

        if (hourOfDay <= 12) {
            tv.setText(String.format(Locale.getDefault(), getContext().getString(R.string.str_greeting), getContext().getString(R.string.str_day), text));
        } else {
            tv.setText(String.format(Locale.getDefault(), getContext().getString(R.string.str_greeting), getContext().getString(R.string.str_afternoon), text));
        }
    }

    private void configureTextNight() {
        if (tv == null)
            return;

        if (textColorNight != null) {
            tv.setTextColor(textColorNight);
        }

        tv.setText(String.format(Locale.getDefault(), getContext().getString(R.string.str_greeting), getContext().getString(R.string.str_night), text));
    }

    public void setInterpolator(TimeInterpolator interpolator) {
        this.interpolator = interpolator;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }
}
